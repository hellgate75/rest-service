package com.continuousdelivery.samples.restservice.acceptance;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/stories")
class CucumberRunner {
    // Intentionally empty
}
