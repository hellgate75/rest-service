pipeline {
    agent any
    triggers {
      pollSCM('H/5 * * * *')
    }
    stages {
      stage('Preparation') { // for display purposes
        steps {
          checkout([$class: 'GitSCM',
              branches: [[name: "origin/master"]],
              doGenerateSubmoduleConfigurations: true,
              extensions: [[$class: 'LocalBranch']],
              submoduleCfg: [],
              userRemoteConfigs: [[
                  credentialsId: "base_credentials",
                  url: "git@bitbucket.org:hellgate75/rest-service.git"
                ]]
              ]
            )
           sh "configure-maven"
        }
      }
      stage('Build') {
         // Run the maven clean and install
         steps {
           sh "mvn clean install"
         }
      }
      stage('Results') {
        steps {
          junit '**/target/surefire-reports/TEST-*.xml'
          archive 'target/*.jar'
        }
      }
      stage('Static Code Analysis') {
         // Run Static code analysis
         steps {
           sh "mvn sonar:sonar -Dsonar.host.url=$SONARQUBE_URL -Dsonar.login=$SONARQUBE_USER -Dsonar.password=$SONARQUBE_PASSWORD -Dsonar.projectKey=rest-service-standalone -Dsonar.projectName=rest-service-stand-alone"
         }
      }
      stage('Deploy Shapshot') {
        // Run the maven build
        steps {
         //  if (isUnix()) {
             sh "mvn deploy -Dmaven.test.skip=true -DaltDeploymentRepository=nexusSnapshots::default::$NEXUS_SNAPSHOT_REPO_URL"
         //  } else {
         //     bat(/mvn deploy -Dmaven.test.skip=true -DaltDeploymentRepository=nexusSnapshots::default::$NEXUS_SNAPSHOT_REPO_URL"/)
         //  }
        }
      }
      stage('Staging Docker Image') {
        // Execute build docker image and push on Staging docker repository
        steps {
          echo 'docker image here ...'
          echo 'This code must run on a DIND Jenkins Agent Node'
        }
      }
      stage('Roll-out in Staging') {
        // Install in Staging environment new docker image
        steps {
          echo 'deploy in staging environment here ...'
        }
      }
      stage('Integration') {
        // Test integration on Staging environment
        steps {
          echo 'integration tests here ...'
        }
      }
      stage('Sign-Off Release') {
        // Sign-off to create release
        steps {
          timeout(time: 72, unit: 'HOURS') {
            input 'Do you wish Release Artifacts on bitbucket?'
          }
        }
      }
      stage('Release Artifacts') {
        // create project release
        steps {
          echo 'activate release here ...'
          // sh "mvn -P release-prod release:clean release:prepare-with-pom release:perform -DdeveloperConnection=scm:git:git@bitbucket.org:digitalrigbitbucketteam/rest-service.git -DreleaseVersion=${releaseVersion} -DdevelopmentVersion=${developmentVersion}"
        }
      }
      stage('Deploy Release') {
        // deploy release on Nexus
        steps {
          echo 'deploy project on release repository ...'
        }
      }
      stage('Production Docker image') {
        // create docker release docker image
        steps {
          echo 'docker image here ...'
          echo 'This code must run on a DIND Jenkins Agent Node'
        }
      }
      stage('Sign-Off Production') {
        // Sign-off roll-out in production
        steps {
          timeout(time: 72, unit: 'HOURS') {
            input 'Do you wish Roll-out to Production?'
          }
        }
      }
      stage('Roll-out in Production') {
        // Roll-out in production
        steps {
          echo 'deploy in staging production here ...'
        }
      }
    }
}
