#!/bin/bash
echo "VANILLA_RECRUITMENT_MODEL_VERSION=$(mvn --projects com.continuousdelivery.samples:rest-service-model  help:evaluate -Dexpression=project.version | grep -e '^[^\[]')" > ./.env
echo "VANILLA_RECRUITMENT_API_VERSION=$(mvn --projects com.continuousdelivery.samples:rest-service-api  help:evaluate -Dexpression=project.version | grep -e '^[^\[]')" >> ./.env
echo "VANILLA_RECRUITMENT_VERSION=$(mvn help:evaluate -Dexpression=project.version | grep -e '^[^\[]')" >> ./.env
echo "DOCKER_RECRUITMENT_VERSION=$(mvn help:evaluate -Dexpression=project.version | grep -e '^[^\[]')" >> ./.env
