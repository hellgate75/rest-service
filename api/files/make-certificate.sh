#!/bin/bash
keytool -genkey -keyalg RSA -alias selfsigned -storepass lbgvanilla -keystore keystore.jks -validity 1660 -keysize 2048
keytool -export -alias selfsigned -keystore keystore.jks -rfc -file certificate.cer
keytool -importkeystore -srckeystore keystore.jks \
   -destkeystore certificate.p12 \
   -srcstoretype jks \
   -deststoretype pkcs12
openssl pkcs12 -in certificate.p12 -out certificate.pem
