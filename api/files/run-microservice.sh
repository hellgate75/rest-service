#!/bin/bash
echo "Vanilla Recruitment Api startup ...."
echo "Execution of app into folder : $APP_ROOT"
echo "Environment : $APP_ENV"
echo "Microservice Version : $MS_VERSION"
echo "Docker Image Version : $RECRUITMENT_DOCKER_VERSION"
echo "Vanilla Recruitment Version : $RECRUITMENT_VERSION"
echo "Vanilla Recruitment Api Version : $RECRUITMENT_API_VERSION"
echo "Vanilla Recruitment Model Version : $RECRUITMENT_MODEL_VERSION"
cd $APP_ROOT
chmod 777 $APP_ROOT/rest-service-api.jar
echo "Starting Microservices ..."
nohup bash -c "java -cp $APP_ROOT -server -Xms$JAVA_MIN_HEAP -Xmx$JAVA_MAX_HEAP $JAVA_OPTS -Dserver.port=${PORT:8080} -jar $APP_ROOT/rest-service-api.jar &> $APP_ROOT/logs/bootstrap.log" &

sleep 2

tail -f $APP_ROOT/logs/bootstrap.log

echo "Microservice unavailable, waiting for maintainance"
tail -f /dev/null
