package com.continuousdelivery.samples.restservice.api

import com.continuousdelivery.samples.restservice.model.HealthCheck
import groovy.util.logging.Slf4j
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Slf4j
class HealthCheckController {
  static final String HEALTHY = 'healthy'


  @RequestMapping("/healthcheck")
  HealthCheck healthcheck() {
    return new HealthCheck(status: HEALTHY)
  }






}
