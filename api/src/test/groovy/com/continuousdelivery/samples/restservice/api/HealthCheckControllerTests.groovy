package com.continuousdelivery.samples.restservice.api

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class HealthCheckControllerTests {

  @Autowired
  MockMvc mockMvc

  @Test
  void shouldReturnStatusHealthy() throws Exception {
    mockMvc.perform(get('/healthcheck')).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath('$.status').value('healthy'));
  }

}
