package com.continuousdelivery.samples.restservice

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class RestServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
