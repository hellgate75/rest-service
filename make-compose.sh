#!/bin/bash

if [[ "--help" == "$1" ]]; then
  echo "make-compose.sh [--rebuild|--help]"
  echo "--rebuild    Rebuild Maven project, recreate docker compose envronment and rebuild, if needed, docker images in compose file"
  echo "--help       Show current help"
  exit 0
fi

if ! [[ -z "$(docker ps -a | grep 'rest-service')" ]]; then
  echo "Compose already created options : "
  echo "docker-compose start"
  echo "or"
  echo "./remove-compose.sh && ./make-compose.sh"
  exit 1
fi

if [[ "--rebuild" == "$1" ]]; then
  echo "Packaging project artifacts via Maven ..."
  mvn clean install
  echo "Updating docker compose environment ..."
  ./update-docker-environment.sh
fi

if [[ -z "$(docker images | grep 'samples/ms-base')" ]]; then
  echo "WARNING: Base Microservices docker image not found!!"
  echo "Build Base Microservices docker image ..."
  cd docker-base
  docker build --rm --force-rm --tag samples/ms-base .
  cd ..
fi
echo "Running docker compose ..."
if [[ "--rebuild" == "$1" ]]; then
  echo "docker compose is running in rebuild mode!!"
  docker-compose up -d --build
else
  docker-compose up -d
fi
